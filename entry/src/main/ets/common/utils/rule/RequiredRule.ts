import { Rule, popMsg } from './Rule'

export class RequiredRule implements Rule {
  validate(val: any, rule: any, form: any): boolean {
    if (rule['required']) {
      if (!val) {
        popMsg(rule['errorMessage'])
        return false;
      }
    }
    return true;
  }
}