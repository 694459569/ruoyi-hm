import { Rule, popMsg } from './Rule'
import { Log } from '../log';

/**
 * 自定义校验函数
 */
export class ValidateFuncRule implements Rule {
  validate(val: any, rule: any, form: any): boolean {
    let func = rule['validateFunction']
    if (func && typeof func === 'function') {
      let pass = func(val, rule, form)
      if (pass) {
        return true
      } else {
        popMsg(rule['errorMessage'])
        return false
      }
    }
    return true
  }
}