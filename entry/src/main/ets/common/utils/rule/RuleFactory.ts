import { RequiredRule } from './RequiredRule'
import { RegExRule } from './RegExRule'
import { FormatRule } from './FormatRule'
import { ValidateFuncRule } from './ValidateFuncRule'
import { Rule } from './Rule'

//校验规则工厂
export const RuleFactory = {
  //注册校验规则
  required: new RequiredRule(),
  pattern: new RegExRule(),
  format: new FormatRule(),
  validateFunction: new ValidateFuncRule(),

  //根据定义的规则获取校验规则
  getRule(rule): Rule {
    let keys = Object.keys(this)
    for (let i = 0;i < keys.length; i++) {
      let key = keys[i];
      if (rule[key]) {
        return this[key]
      }
    }
    return null;
  }
}