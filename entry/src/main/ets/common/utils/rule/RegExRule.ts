import { Rule, popMsg } from './Rule'
import { Log } from '../log';
/**
 * 正则表达式类型校验
 */
export class RegExRule implements Rule {
  validate(val: any, rule: any, form: any): boolean {
    let exp = rule['pattern']
    if (exp) {
      try {
        let regEx = new RegExp(exp)
        if (!regEx.test(val)) {
          popMsg(rule['errorMessage'])
          return false;
        }
      } catch (e) {
        Log.error('校验规则非正则表达式忽略', exp, e)
      }
    }
    return true;
  }
}