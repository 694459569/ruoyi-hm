import { Log } from '../utils/log'
import { getInfo } from '../../api/login'
import { getToken, setToken, setUserState, delUserState, getUserState } from '../store'
import { globalCfg } from '../../config/setting'

export class UserState {
  token: any;
  name: string;
  avatar: string;
  roles: any[];
  permissions: string[];

  constructor() {
    this.token = getToken()
  }

  //加载登录用户信息
  async loadUserInfo() {
    try {
      let res: any = await getInfo()
      this.load(this, res)
      //缓存用户状态信息
      setUserState(this);
      Log.info('加载登录用户信息成功', JSON.stringify(res))
    } catch (e) {
      Log.error('加载登录用户信息发生异常', e)
    }
  }

  //刷新登录用户信息
  async refreshUserInfo() {
    try {
      let res: any = await getInfo()
      this.load(this, res)
      Log.info('刷新登录用户信息成功', JSON.stringify(res))
    } catch (e) {
      Log.error('刷新登录用户信息发生异常', e)
    }
  }
  //注销当前登录用户
  logout() {
    setToken('');
    let userState = getUserState()
    if (userState) {
      userState.clear()
    }
  }

  private load(userState: UserState, res: any) {
    const user = res.user;
    const avatar = user.avatar === '' || user.avatar == null ? '' : globalCfg.baseUrl + user.avatar;
    if (res.roles && res.roles.length > 0) {
      // 验证返回的roles是否是一个非空数组
      userState.roles = res.roles;
      userState.permissions = res.permissions;
    } else {
      userState.roles = ['ROLE_DEFAULT'];
    }
    userState.name = user.userName;
    userState.avatar = avatar;
  }

  clear() {
    this.token = ''
    this.name = ''
    this.avatar = ''
    this.roles = null
    this.permissions = null
  }
}