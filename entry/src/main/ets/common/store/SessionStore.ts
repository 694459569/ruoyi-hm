import { Log } from '../utils/log'

/**
 * Cookie数据库
 */
export default class SessionStore {
  static set(key: string, val: any) {
    try {
      AppStorage.SetOrCreate<any>(key, val)
      PersistentStorage.PersistProp<any>(key, val);
    } catch (e) {
      Log.error(`sessionStore保存数据发生异常. Code:${e.code},message:${e.message}`);
    }
  }

  static get(key: string): any {
    try {
      return AppStorage.Get<any>(key);
    } catch (e) {
      Log.error(`sessionStore获取数据发生异常. Code:${e.code},message:${e.message}`);
    }
  }

  static remove(key: string) {
    try {
      AppStorage.Delete(key)
      PersistentStorage.DeleteProp(key)
    } catch (e) {
      Log.error(`sessionStore删除数据发生异常. Code:${e.code},message:${e.message}`);
    }
  }
}