import router from '@ohos.router';
import display from '@ohos.display';
import fs from '@ohos.file.fs';
import { Log } from '../../common/utils/log';
import { getPixelMap } from '../../common/utils/file'
import { CommonConstants } from '../../common/constants/CommonConstants';
import { square } from '../../common/utils/crop/cropUtil'

@Entry
@Component
struct AvatarPage {
  DISPLAY = display.getDefaultDisplaySync()
  SCREEN_WIDTH = this.DISPLAY.width
  PAGE_X // 手按下的x位置
  PAGE_Y // 手按下y的位置
  PR = this.DISPLAY.densityDPI // dpi
  T_PAGE_X // 手移动的时候x的位置
  T_PAGE_Y // 手移动的时候Y的位置
  CUT_L // 初始化拖拽元素的left值
  CUT_T // 初始化拖拽元素的top值
  CUT_R // 初始化拖拽元素的
  CUT_B // 初始化拖拽元素的
  CUT_W // 初始化拖拽元素的宽度
  CUT_H //  初始化拖拽元素的高度
  IMG_RATIO // 图片比例
  IMG_REAL_W // 图片实际的宽度
  IMG_REAL_H // 图片实际的高度
  DRAFG_MOVE_RATIO = 1 //移动时候的比例,
  INIT_DRAG_POSITION = 100 // 初始化屏幕宽度和裁剪区域的宽度之差，用于设置初始化裁剪的宽度
  DRAW_IMAGE_W = this.SCREEN_WIDTH // 设置生成的图片宽度

  // 初始化的宽高
  cropperInitW = this.SCREEN_WIDTH
  cropperInitH = this.SCREEN_WIDTH
  // 动态的宽高
  @State cropperW: number = this.SCREEN_WIDTH
  @State cropperH: number = this.SCREEN_WIDTH
  // 动态的left top值
  cropperL = 0
  cropperT = 0
  transL = 0
  transT = 0

  // 图片缩放值
  scaleP = 0
  imageW = 0
  imageH = 0

  // 裁剪框 宽高
  @State cutL: number = 0
  @State cutT: number = 0
  cutB = this.SCREEN_WIDTH
  cutR: any = '100%'
  qualityWidth = this.DRAW_IMAGE_W
  innerAspectRadio = this.DRAFG_MOVE_RATIO
  @State loading: boolean = false;
  @State hidden: boolean = false;
  @State pixelMapChange: boolean = false;
  @State rect: any = {
    x: 0,
    y: 0,
    width: 512,
    height: 512,
    fillOpacity: 0.1,
    stroke: $r('sys.color.ohos_id_color_palette9'),
    strokeWidth: 1
  }
  @State pixel: any = {
    pixelMap: null,
    width: null,
    height: null,
    density: null,
  }
  @State offsetX: number = 0;
  @State offsetY: number = 0;

  calculate() {
    this.IMG_RATIO = 1 / 1
    if (this.IMG_RATIO >= 1) {
      this.IMG_REAL_W = this.SCREEN_WIDTH
      this.IMG_REAL_H = this.SCREEN_WIDTH / this.IMG_RATIO
    } else {
      this.IMG_REAL_W = this.SCREEN_WIDTH * this.IMG_RATIO
      this.IMG_REAL_H = this.SCREEN_WIDTH
    }
    let minRange = this.IMG_REAL_W > this.IMG_REAL_H ? this.IMG_REAL_W : this.IMG_REAL_H
    this.INIT_DRAG_POSITION = minRange > this.INIT_DRAG_POSITION ? this.INIT_DRAG_POSITION : minRange
    // 根据图片的宽高显示不同的效果   保证图片可以正常显示
    if (this.IMG_RATIO >= 1) {
      let cutT = Math.ceil((this.SCREEN_WIDTH / this.IMG_RATIO - (this.SCREEN_WIDTH / this.IMG_RATIO - this.INIT_DRAG_POSITION)) / 2)
      let cutB = cutT
      let cutL = Math.ceil((this.SCREEN_WIDTH - this.SCREEN_WIDTH + this.INIT_DRAG_POSITION) / 2)
      let cutR = cutL

      this.cropperW = this.SCREEN_WIDTH
      this.cropperH = this.SCREEN_WIDTH / this.IMG_RATIO
      // 初始化left right
      this.cropperL = Math.ceil((this.SCREEN_WIDTH - this.SCREEN_WIDTH) / 2)
      this.cropperT = Math.ceil((this.SCREEN_WIDTH - this.SCREEN_WIDTH / this.IMG_RATIO) / 2)
      this.cutL = cutL
      this.cutT = cutT
      this.cutR = cutR
      this.cutB = cutB
      // 图片缩放值
      this.imageW = this.IMG_REAL_W
      this.imageH = this.IMG_REAL_H
      this.scaleP = this.IMG_REAL_W / this.SCREEN_WIDTH
      this.qualityWidth = this.DRAW_IMAGE_W
      this.innerAspectRadio = this.IMG_RATIO

    } else {
      let cutL = Math.ceil((this.SCREEN_WIDTH * this.IMG_RATIO - (this.SCREEN_WIDTH * this.IMG_RATIO)) / 2)
      let cutR = cutL
      let cutT = Math.ceil((this.SCREEN_WIDTH - this.INIT_DRAG_POSITION) / 2)
      let cutB = cutT
      this.cropperW = this.SCREEN_WIDTH * this.IMG_RATIO
      this.cropperH = this.SCREEN_WIDTH
      // 初始化left right
      this.cropperL = Math.ceil((this.SCREEN_WIDTH - this.SCREEN_WIDTH * this.IMG_RATIO) / 2)
      this.cropperT = Math.ceil((this.SCREEN_WIDTH - this.SCREEN_WIDTH) / 2)

      this.cutL = cutL
      this.cutT = cutT
      this.cutR = cutR
      this.cutB = cutB
      // 图片缩放值
      this.imageW = this.IMG_REAL_W
      this.imageH = this.IMG_REAL_H
      this.scaleP = this.IMG_REAL_W / this.SCREEN_WIDTH
      this.qualityWidth = this.DRAW_IMAGE_W
      this.innerAspectRadio = this.IMG_RATIO
    }
    Log.info('初始化计算', this.SCREEN_WIDTH, this.cutT, this.cutL, this.cutB, this.cutR)
  }

  onPageShow() {
    this.calculate()
    this.rect.x = this.cutL
    this.rect.y = this.cutT

    let params = router.getParams();
    let uri = params['uri'];
    let file = fs.openSync(uri, fs.OpenMode.READ_ONLY);
    getPixelMap(file.fd).then(px => {
      this.pixel.pixelMap = px;
      px.getImageInfo().then(info => {
        this.pixel.width = info.size.width;
        this.pixel.height = info.size.height;
        this.pixel.density = info.density
      })
      fs.close(file);
    }).catch(e => {
      fs.close(file);
    });
  }

  @Builder
  pixelMapBuilder() {
    Rect()
      .width(this.rect.width + 'px')
      .height(this.rect.height + 'px')
      .fillOpacity(this.rect.fillOpacity)
      .stroke(this.rect.stroke)
      .strokeWidth(this.rect.strokeWidth)
  }
  //显示按钮
  @Builder
  renderBt() {
    Button() {
      Row() {
        if (this.loading) {
          Image($r('app.media.loading'))
            .width(40)
            .height(40)
            .margin({ right: 5 })
          Text('保存中...').fontSize($r('app.float.text_input_font_size'))
            .fontWeight(CommonConstants.LOGIN_TEXT_FONT_WEIGHT)
            .fontColor(this.isButtonClickable() ? Color.White : $r('app.color.login_font_normal'))
        } else {
          Text('保存')
            .fontSize($r('app.float.text_input_font_size'))
            .fontWeight(CommonConstants.LOGIN_TEXT_FONT_WEIGHT)
            .fontColor(this.isButtonClickable() ? Color.White : $r('app.color.login_font_normal'))
        }
      }.alignItems(VerticalAlign.Center)
    }
    .width('95%')
    .height($r('app.float.login_btn_height'))
    .borderRadius($r('app.float.login_btn_border_radius'))
    .margin({ top: $r('app.float.register_btn_margin_top') })
    .enabled(this.isButtonClickable())
    .backgroundColor(this.isButtonClickable() ? $r('app.color.login_btn_enabled') : $r('app.color.login_btn_normal'))
    .onClick(() => {
      let cropW = (this.rect.width/ this.cropperW) * this.IMG_REAL_W
      let cropH = (this.rect.height / this.cropperH) * this.IMG_REAL_H
      let cropX = (this.cutL / this.cropperW) * this.IMG_REAL_W
      let cropY = (this.cutT / this.cropperH) * this.IMG_REAL_H
      //let cropX = (this.rect.x * this.pixel.density) / 160
      //let cropY = (this.rect.y * this.pixel.density) / 160
      Log.info('开始剪裁图片', cropW, cropH, cropX, cropY)
      square(this.pixel.pixelMap, cropW, cropH, cropX, cropY).then(() => {
        Log.info('图片剪裁成功')
        this.pixelMapChange = true;
      }).catch(e => {
        Log.error('图片剪裁失败', e)
      })
    })
  }

  build() {
    Column() {
      Stack() {
        Image(this.pixel.pixelMap)
          //.width('100%')
          .width(this.cropperW + 'px')
          .height(this.cropperH + 'px')
          .objectFit(ImageFit.Fill)
        if (this.pixelMapChange) {
          Image(this.pixel.pixelMap).width('256px').height('256px').objectFit(ImageFit.Fill)
        }
        Rect()
          .width(this.rect.width + 'px')
          .height(this.rect.height + 'px')
          .fillOpacity(this.rect.fillOpacity)
          .stroke(this.rect.stroke)
          .strokeWidth(this.rect.strokeWidth)
          .position({ x: this.rect.x, y: this.rect.y })
          .visibility(this.hidden ? Visibility.None : Visibility.Visible)
          .onTouch((event: TouchEvent) => {
            if (event.type === TouchType.Down) {

            }
          })
          .onDragStart((event: DragEvent) => { // 第一次拖拽此事件绑定的组件时，触发回调。
            this.offsetX = event.getX() - this.rect.x
            this.offsetY = event.getY() - this.rect.y
            this.PAGE_X = event.getX()
            this.PAGE_Y = event.getY()
            this.hidden = true // 控制显隐
            return this.pixelMapBuilder() // 设置拖拽过程中显示的图片。
          })
      }
      //.width('100%')
      //.height('90%')
      .alignContent(Alignment.TopStart)
      //.backgroundColor(Color.Red)
      .onDrop((event: DragEvent, extraParams: string) => { // 绑定此事件的组件可作为拖拽释放目标，当在本组件范围内停止拖拽行为时，触发回调。
        //this.rect.x = event.getX() - this.offsetX
        //this.rect.y = event.getY() - this.offsetY
        this.hidden = false
      })
      .onDragMove((event: DragEvent, extraParams: string) => {
        var dragLengthX = (this.PAGE_X - event.getX()) * this.DRAFG_MOVE_RATIO
        var dragLengthY = (this.PAGE_Y - event.getY()) * this.DRAFG_MOVE_RATIO
        // 左移
        if (dragLengthX > 0) {
          if (this.cutL - dragLengthX < 0) dragLengthX = this.cutL
        } else {
          //if (this.cutR + dragLengthX < 0) dragLengthX = -this.cutR
        }

        if (dragLengthY > 0) {
          if (this.cutT - dragLengthY < 0) dragLengthY = this.cutT
        } else {
          //if (this.cutB + dragLengthY < 0) dragLengthY = -this.cutB
        }

        this.cutL = this.cutL - dragLengthX
        this.cutT = this.cutT - dragLengthY
        //this.cutR = this.cutR + dragLengthX
        //this.cutB = this.cutB + dragLengthY

        this.PAGE_X = event.getX()
        this.PAGE_Y = event.getY()

        this.rect.x = this.cutL
        this.rect.y = this.cutT

        //Log.info('计算拖拽', this.PAGE_X, this.PAGE_Y, this.cutL, this.cutT, dragLengthX, dragLengthY)
      })
      .onDragLeave((event: DragEvent, extraParams: string) => {
        //this.hidden = false
        //this.rect.x = 0
        //this.rect.y = 0
      })

      this.renderBt()
    }
    .height('100%')
    .width('100%')
    .justifyContent(FlexAlign.Start)
  }

  isButtonClickable(): boolean {
    if (this.loading) {
      return false;
    }
    return true;
  }
}