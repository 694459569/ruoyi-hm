import NavTitleComponent from '../../view/NavTitleComponent'
import { global } from '../../config/theme'
import { updateUserPwd } from '../../api/user'
import { popMsg } from '../../common/utils/ruoyi';
import { Log } from '../../common/utils/log';
import { FormComp, FormField, FieldType, Form } from '../../view/FormComponent'
import { SubmitBtComp } from '../../view/ButtonComponent'
/**
 * 修改密码
 */
@Entry
@Component
struct UpdatePwdPage {
  @State loading: boolean = false;
  @State @Watch('handleOnSubmit') onSubmit: boolean = false;
  @State title: string = '修改密码';
  @State form: Form = null;
  @State rules: object = {
    oldPassword: [
      {
        required: true,
        errorMessage: '旧密码不能为空'
      }
    ],
    newPassword: [
      {
        required: true,
        errorMessage: '新密码不能为空'
      }
    ],
    confirmPassword: [
      {
        required: true,
        errorMessage: '确认密码不能为空'
      },
      {
        validateFunction: (value, rule, form) => form.newPassword === value,
        errorMessage: '两次输入的密码不一致'
      }
    ],
  }

  onPageShow() {
    this.initForm()
  }

  initForm() {
    this.form = new Form()
      .setRules(this.rules)
      .add(new FormField('旧密码', 'oldPassword', '', true, FieldType.password))
      .add(new FormField('新密码', 'newPassword', '', true, FieldType.password))
      .add(new FormField('确认密码', 'confirmPassword', '', true, FieldType.password))
  }

  handleOnSubmit() {
    if (this.onSubmit) {
      if (this.form.validate()) {
        this.loading = true
        Log.info('form校验通过')
        let formVal = this.form.getFormVals()
        updateUserPwd(formVal.oldPassword, formVal.newPassword).then(res => {
          this.loading = false
          this.onSubmit = false
          popMsg('修改成功')
        }).catch(e => {
          this.loading = false
          this.onSubmit = false
        })
      } else {
        this.loading = false
        this.onSubmit = false
      }
    }
  }

  //显示标题
  @Builder
  renderTitle() {
    Column() {
      NavTitleComponent({
        title: this.title,
        back: true,
        backUrl: '',
      })
    }
    .width('100%')
    .height('100%')
  }

  //显示表单
  @Builder
  renderForm() {
    FormComp({ form: this.form })
  }

  //显示保存按钮
  @Builder
  renderBt() {
    SubmitBtComp({
      loading: $loading,
      onSubmit: $onSubmit,
      activeLabel: '保存',
      deactiveLabel: '保存中...',
      _width: '95%'
    })
  }

  //显示页面主体
  @Builder
  renderBody() {
    Column() {
      this.renderForm();
      this.renderBt();
    }
    .height('100%')
    .width('100%')
    .backgroundColor(global.bodyBackgroundColor)
  }

  build() {
    Navigation() {
      this.renderBody()
    }
    .title(this.renderTitle())
  }

  isButtonClickable(): boolean {
    if (this.loading) {
      return false;
    }
    return true;
  }
}